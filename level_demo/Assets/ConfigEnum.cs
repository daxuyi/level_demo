namespace Common
{
    public enum ElementType
    {
        Normal, // 普通
        Floating, // 漂浮类物品（云）
        Egg, // 蛋
        BuildSite, // 地基
        BuildHouse, // 龙房子
        BuildCoin, // 金币房子
        BuildWood, // 木材房子
        Gem, // 钻石
        Coin, // 金币
        Wood, // 木材
        Point, // 积分物品
        CoinPile, // 金币堆
        WoodPile, // 木材堆
        Box, // 宝箱
        Jar, // 合成管
        TransPortal, // 传送门
        Power, // 巨龙之力物品
        ElementUp, // 可升级物品
        ElementPackage, // 物品包装
        BlessingValue, // 治疗之力物品
        BlessingWell, // 治疗井
        BlessingStuff, // 愈合剂
        NpcAutoProducer, // 自动产出为Npc的物品
        NpcClickProducer, // 点击产出为Npc的物品
        NpcProducer, // 所有产出为npc的物品
        EventPoint, // 活动积分物品
        UnlockFog, // 解锁迷雾物品
        ElementLoot,// 地上战利品球
    }
    
    public enum ClickChangeType
    {
        None,
        NumDec,
        ItemChange,
        NumChange
    }

    public enum AutoProductType
    {
        None,
        NumDec,
        ItemChange,
        NumChange,
    }

    public enum BoxType
    {
        Normal,
        Gem,
        Cash
    }

    public enum ElementUpType
    {
        Gem = 0
    }

    public enum RewardType
    {
        Element,
        NPC
    }
    
    // todo
    public enum ChainType
    {
        
    }

    public enum NpcType
    {
        Light = 1,
        Fire,
        Water,
        Dark,
        Wind,
        Earth,
        Enemy,
    }

    public enum MergeType
    {
        Npc,
        Nest
    }

    public enum QuestType
    {
        MergeElement,
        MergeNpc,
        MergeElement5,
        MergeNpc5,
        MergeElementAny,
        MergeNpcAny,
        MergeElementAny5,
        MergeNpcAny5,
        MergeEgg,
        MergeEgg5,
        MergeNpcLevel,
        MergeNpcLevel5,
        MergeDeadLand,
        MergeDeadLandAny,
        GainElement,
        GainNpc,
        GainNpcLevel,
        GainCoin,
        GainWood,
        GainGem,
        GainPoint,
        CollectElementForce,
        CollectElement,
        StatusElement,
        StatusNpc,
        StatusNest,
        StatusLand,
        StatusPoint,
        StatusPower,
        ClickProduce,
        ClickFloating,
        ClickOrb,
        ClickBox,
        ClickLifeOrb,
        ClickCoin,
        ClickWood,
        ClickGem,
        UnlockLand,
        UnlockAllLand,
        UnlockFog,
        FinishLevel,
        AutoChange,
        PurchaseInTrade,
        PurchaseInShop,
        CostCoin,
        CostWood,
        CostGem,
        UseBlessingStuff,
        AttackElement,
        AttackNpc,
        ClickNest
    }

    public enum QuestGuideType
    {
        None,
        Element,
        Chain,
        ShopTrade,
        Egg,
        Nest,
        Npc,
        ShopTreasure,
        ShopEgg,
        Level,
        Fog,
        Floating,
        Orb,
    }

    public enum ElementProductType
    {
        Common,
        Level
    }
    
    public enum ShopType
    {
        Npc = 0,
        Resource = 1,
        Build = 2,
    }

    public enum ShopPriceType
    {
        Wood = 0,
        Coin = 1,
        Gem = 2
    }

    public enum ShopPriceRule
    {
        None,
        Building,
        Normal
    }

    public enum ChargePlatform
    {
        GooglePlay,
        AppStore
    }

    public enum ChargeType
    {
        Consumable,
        NonConsumable,
        Subscription,
    }

    public enum ChargeClassification
    {
        Cost,
        Gem,
        Subs,
        Box,
        Land,
        Price,
        Event,
    }

    public enum ShopSceneType
    {
        Camp,
        Level,
        Event,
    }
    
    public enum ShopTradeOrder
    {
        Order1,
        Order2,
        Order3,
        Lock,
    }

    public enum RuleFeatureName
    {
        DailyTrade,
        DailyChest,
        Shop,
        ShopTrade,
        ShopBuild,
        ShopNpc,
        ShopResource,
        NPCPower,
        Level,
        Chain,
        Book,
        CampQuest,
        Floating,
        SaleButton,
        AutoCollect,
        EventEntrance,
        EventAttend,
        ElementGuide
    }

    public enum LevelType
    {
        Main,
        Branch,
        Secret,
    }
}