﻿using Config;
using TMPro;
using UnityEngine;

namespace Game.UI.Level.LevelMapNew
{
    public class LevelMapItem
    {
        enum LoadStatus
        {
            None = 0,
            Loading = 1,
            Finish = 2,
        }

        public string PrefabName { get; }
        public Vector2 LocalPosition { get; }
        public float Height { get; }

        private bool _isLoadManual; //标记 LoadPrefab 方法是否被调用过了
        private LoadStatus _loadingStatus;

        private readonly Transform _parent;

        private GameObject _go;

        
        public LevelMapItem(string prefabName, Transform parent, Vector2 localPosition, float height)
        {
            PrefabName = prefabName;
            LocalPosition = localPosition;
            Height = height;
            _isLoadManual = false;
            _loadingStatus = LoadStatus.None;
            _parent = parent;
        }


        private int GetLevelIdByPrefabAndPointIndex(string prefabName, int pointIndex)
        {
            var confData = ConfLevel.Data;
            foreach (var confLevel in confData)
            {
                if (confLevel.Value.PrefabUse == PrefabName && confLevel.Value.PrefabInOrder == pointIndex)
                {
                    return confLevel.Key;
                }
            }

            return 0;
        }
    }
}