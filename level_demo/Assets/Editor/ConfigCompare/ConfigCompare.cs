﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;
using UnityEditor;
using Config;
using UnityEditor.VersionControl;


public class ConfigCompare : EditorWindow
{
    private static ConfigCompare _configCompare;
    private static TextAsset _oldConfigFile;
    private static TextAsset _newConfigFile;
    private static string _directoryPath;
    private static ConfigData _oldData;
    private static ConfigData _newData;
    private static string _oldConfigPath;
    private static string _newConfigpath;
    private static List<FieldInfo> _tagedFields;


    public class ConfigData
    {
        public IDictionary<int, ConfDailyTrade> ConfDailyTradeData;
        public IDictionary<int, ConfELementProduct> ConfELementProducts;
        public IDictionary<int, ConfElement> ConfElements;
        public IDictionary<int, ConfElementJar> ConfElementJars;
        public IDictionary<int, ConfElementBox> ConfElementBoxes;
        public IDictionary<int, ConfElementUp> ConfElementUps;
        public IDictionary<int, ConfELementChain> ConfELementChains;
        public IDictionary<int, ConfEventParameter> ConfEventParameters;
        public IDictionary<int, ConfEvent> ConfEvents;
        public IDictionary<int, ConfFog> ConfFogs;
        public IDictionary<int, ConfGuide> ConfGuides;
        public IDictionary<int, ConfTalk> ConfTalks;
        public IDictionary<int, ConfLevelMap> ConfLevelMaps;
        public IDictionary<int, ConfLevel> ConfLevels;
        public IDictionary<int, ConfNpc> ConfNpcs;
        public IDictionary<int, ConfNpcBook> ConfNpcBooks;
        public IDictionary<int, ConfQuest> ConfQuests;
        public IDictionary<int, ConfRuleCombo> ConfRuleCombos;
        public IDictionary<int, ConfRuleLoadingTips> ConfRuleLoadingTipses;
        public IDictionary<int, ConfRuleParameter> ConfRuleParameters;
        public IDictionary<int, ConfRuleDailyBox> ConfRuleDailyBoxes;
        public IDictionary<int, ConfRuleLevelBox> ConfRuleLevelBoxes;
        public IDictionary<int, ConfRuleFeature> ConfRuleFeatures;
        public IDictionary<int, ConfRuleLandPoints> ConfRuleLandPointses;
        public IDictionary<int, ConfShop> ConfShops;
        public IDictionary<int, ConfShopTrade> ConfShopTrades;
        public IDictionary<int, ConfShopCharge> ConfShopCharges;
    }


    [MenuItem("/ConfigCompare/Compare")]
    protected static void ShowWindow()
    {
        // _configCompare =
        //     (ConfigCompare) EditorWindow.GetWindow(typeof(ConfigCompare), false, "CreateUIElements", false);
        // _configCompare.maxSize = new Vector2(500, 500);
        // _configCompare.Show();

        if (ConfigLoader.LoadItems == null)
        {
            Configs.Init();
        }

        _oldData = new ConfigData();
        _newData = new ConfigData();
        _tagedFields = new List<FieldInfo>();
        _oldConfigPath = EditorUtility.OpenFilePanel("请选择修改前的配置文件", Directory.GetCurrentDirectory(), "bytes");
        _newConfigpath = EditorUtility.OpenFilePanel("请选择修改后的配置文件", Directory.GetCurrentDirectory(), "bytes");
        if (_oldConfigPath != null && _newConfigpath != null)
        {
            LoadConfig(File.ReadAllBytes(_oldConfigPath));
            SaveConfig(_oldData);
            LoadConfig(File.ReadAllBytes(_newConfigpath));
            SaveConfig(_newData);
            Compare();
        }
        else
        {
            Debug.LogError("请选择有效的路径!!!");
        }
    }


    private static bool LoadConfig(byte[] datas)
    {
        using (var ba = new ByteArray(datas))
        {
            while (ba.ReadAvailable)
            {
                var file = ba.ReadUTF();
                var count = ba.ReadInt();
                var length = ba.ReadInt();
                var bytes = ba.ReadBytes(length);

                var loaderItem = ConfigLoader.GetLoaderItem(file);
                if (loaderItem != null && length > 0)
                {
                    ConfigLoader.CreateConfig(bytes, loaderItem.ItemFile, count, loaderItem.Callback);
                }
                else
                {
                    if (loaderItem == null)
                    {
                        Debug.LogWarning($"Config {file} cannot load!!!");
                    }

                    if (length == 0)
                    {
                        Debug.LogWarning($"Config {file} has no data!!!");
                    }
                }
            }
        }

        return true;
    }

    private static void SaveConfig(ConfigData configData)
    {
        configData.ConfDailyTradeData = ConfDailyTrade.Data;
        configData.ConfELementProducts = ConfELementProduct.Data;
        configData.ConfElements = ConfElement.Data;
        configData.ConfElementJars = ConfElementJar.Data;
        configData.ConfElementBoxes = ConfElementBox.Data;
        configData.ConfElementUps = ConfElementUp.Data;
        configData.ConfELementChains = ConfELementChain.Data;
        configData.ConfEventParameters = ConfEventParameter.Data;
        configData.ConfEvents = ConfEvent.Data;
        configData.ConfFogs = ConfFog.Data;
        configData.ConfGuides = ConfGuide.Data;
        configData.ConfTalks = ConfTalk.Data;
        configData.ConfLevelMaps = ConfLevelMap.Data;
        configData.ConfLevels = ConfLevel.Data;
        configData.ConfNpcs = ConfNpc.Data;
        configData.ConfNpcBooks = ConfNpcBook.Data;
        configData.ConfQuests = ConfQuest.Data;
        configData.ConfRuleCombos = ConfRuleCombo.Data;
        configData.ConfRuleLoadingTipses = ConfRuleLoadingTips.Data;
        configData.ConfRuleParameters = ConfRuleParameter.Data;
        configData.ConfRuleDailyBoxes = ConfRuleDailyBox.Data;
        configData.ConfRuleLevelBoxes = ConfRuleLevelBox.Data;
        configData.ConfRuleFeatures = ConfRuleFeature.Data;
        configData.ConfRuleLandPointses = ConfRuleLandPoints.Data;
        configData.ConfShops = ConfShop.Data;
        configData.ConfShopTrades = ConfShopTrade.Data;
        configData.ConfShopCharges = ConfShopCharge.Data;
    }

    private static void Compare()
    {
        FileDirectoryBuild();
        ConfCompare(_oldData.ConfDailyTradeData, _newData.ConfDailyTradeData, "DailyTrade.md");
        ConfCompare(_oldData.ConfELementProducts, _newData.ConfELementProducts, "ELementProduct.md");
        ConfCompare(_oldData.ConfElements, _newData.ConfElements, "Element.md");
        ConfCompare(_oldData.ConfElementJars, _newData.ConfElementJars, "ElementJar.md");
        ConfCompare(_oldData.ConfElementBoxes, _newData.ConfElementBoxes, "ElementBox.md");
        ConfCompare(_oldData.ConfElementUps, _newData.ConfElementUps, "ElementUp.md");
        ConfCompare(_oldData.ConfELementChains, _newData.ConfELementChains, "ELementChain.md");
        ConfCompare(_oldData.ConfEventParameters, _newData.ConfEventParameters, "EventParameter.md");
        ConfCompare(_oldData.ConfEvents, _newData.ConfEvents, "Event.md");
        ConfCompare(_oldData.ConfFogs, _newData.ConfFogs, "Fog.md");
        ConfCompare(_oldData.ConfGuides, _newData.ConfGuides, "Guide.md");
        ConfCompare(_oldData.ConfTalks, _newData.ConfTalks, "Talk.md");
        ConfCompare<ConfLevelMap>(_oldData.ConfLevelMaps, _newData.ConfLevelMaps, "LevelMap.md");
        ConfCompare<ConfLevel>(_oldData.ConfLevels, _newData.ConfLevels, "Level.md");
        ConfCompare<ConfNpc>(_oldData.ConfNpcs, _newData.ConfNpcs, "Npc.md");
        ConfCompare<ConfNpcBook>(_oldData.ConfNpcBooks, _newData.ConfNpcBooks, "NpcBook.md");
        ConfCompare<ConfQuest>(_oldData.ConfQuests, _newData.ConfQuests, "Quest.md");
        ConfCompare<ConfRuleCombo>(_oldData.ConfRuleCombos, _newData.ConfRuleCombos, "RuleCombo.md");
        ConfCompare<ConfRuleLoadingTips>(_oldData.ConfRuleLoadingTipses, _newData.ConfRuleLoadingTipses,
            "RuleLoadingTips.md");
        ConfCompare<ConfRuleParameter>(_oldData.ConfRuleParameters, _newData.ConfRuleParameters, "RuleParameter.md");
        ConfCompare<ConfRuleDailyBox>(_oldData.ConfRuleDailyBoxes, _newData.ConfRuleDailyBoxes, "RuleDailyBox.md");
        ConfCompare<ConfRuleLevelBox>(_oldData.ConfRuleLevelBoxes, _newData.ConfRuleLevelBoxes, "RuleLevelBox.md");
        ConfCompare<ConfRuleFeature>(_oldData.ConfRuleFeatures, _newData.ConfRuleFeatures, "RuleFeature.md");
        ConfCompare<ConfRuleLandPoints>(_oldData.ConfRuleLandPointses, _newData.ConfRuleLandPointses,
            "RuleLandPoints.md");
        ConfCompare<ConfShop>(_oldData.ConfShops, _newData.ConfShops, "Shop.md");
        ConfCompare<ConfShopTrade>(_oldData.ConfShopTrades, _newData.ConfShopTrades, "ShopTrade.md");
        ConfCompare<ConfShopCharge>(_oldData.ConfShopCharges, _newData.ConfShopCharges, "ShopCharge.md");
    }

    private static void FileDirectoryBuild()
    {
        string corpath = Directory.GetCurrentDirectory();
        _directoryPath = corpath + "/CompareResult";
        if (!Directory.Exists(_directoryPath))
        {
            Directory.CreateDirectory(_directoryPath);
        }
    }


    private static void ConfCompare<T>(IDictionary<int, T> oldConfigDataClass, IDictionary<int, T> newConfigDataClass,
        string path)
    {
        if (oldConfigDataClass == null || newConfigDataClass == null)
        {
            Debug.Log(typeof(T).Name + "为空");
        }

        path = _directoryPath + "/" + path;
        StreamWriter writer;
        FileStream file;
        bool existFlag = false; //是否需要创建该文件
        if (!File.Exists(path))
        {
            file = File.Create(path);
        }
        else
        {
            File.Delete(path);
            file = File.Create(path);
        }

        writer = new StreamWriter(file);
        var type = typeof(T);
        var fields = type.GetFields();

        existFlag = PrintDeletedData(fields, writer, oldConfigDataClass, newConfigDataClass);
        existFlag = PrintAddedData(fields, writer, oldConfigDataClass, newConfigDataClass) || existFlag;
        existFlag = PrintChangedData(fields, writer, oldConfigDataClass, newConfigDataClass) || existFlag;

        writer.Close();
        file.Close();

        if (!existFlag)
        {
            File.Delete(path);
        }
    }

    private static void PrintOneToFile<T>(FieldInfo[] fields, StreamWriter writer, IDictionary<int, T> configDataClass,
        int key)
    {
        writer.Write("|");
        foreach (FieldInfo field in fields)
        {
            if (field.FieldType.Name == "Int32[]")
            {
                if (_tagedFields.Contains(field))
                {
                    writer.Write("<table><td bgcolor=red>" + GetIntList(field.GetValue(configDataClass[key]) as int[]) +
                                 "</td></table>" + "|");
                }
                else
                {
                    writer.Write(GetIntList(field.GetValue(configDataClass[key]) as int[]) + "|");
                }
            }
            else if (field.FieldType.Name == "ConfigIdCount[]")
            {
                if (_tagedFields.Contains(field))
                {
                    writer.Write("<table><td bgcolor=red>" +
                                 GetConfigIdCountList(field.GetValue(configDataClass[key]) as ConfigIdCount[]) +
                                 "</td></table>" + "|");
                }
                else
                {
                    writer.Write(GetConfigIdCountList(field.GetValue(configDataClass[key]) as ConfigIdCount[]) + "|");
                }
            }
            else if (field.FieldType.Name == "ConfigProbCount[]")
            {
                if (_tagedFields.Contains(field))
                {
                    writer.Write("<table><td bgcolor=red>" + GetConfigProbCountList(
                                     field.GetValue(configDataClass[key]) as ConfigProbCount[]) +
                                 "</td></table>" + "|");
                }
                else
                {
                    writer.Write(
                        GetConfigProbCountList(field.GetValue(configDataClass[key]) as ConfigProbCount[]) + "|");
                }
            }
            else if (field.FieldType.Name == "Single[]")
            {
                if (_tagedFields.Contains(field))
                {
                    writer.Write("<table><td bgcolor=red>" + GetFloatList(
                                     field.GetValue(configDataClass[key]) as float[]) +
                                 "</td></table>" + "|");
                }
                else
                {
                    writer.Write(
                        GetFloatList(
                            field.GetValue(configDataClass[key]) as float[]) + "|");
                }
            }
            else if (field.FieldType.Name == "ConfigIdCount")
            {
                if (_tagedFields.Contains(field))
                {
                    writer.Write("<table><td bgcolor=red>" + GetConfigIdCount(
                                     field.GetValue(configDataClass[key]) as ConfigIdCount) +
                                 "</td></table>" + "|");
                }
                else
                {
                    writer.Write(
                        GetConfigIdCount(
                            field.GetValue(configDataClass[key]) as ConfigIdCount) + "|");
                }
            }
            else if (field.Name != "Data")
            {
                if (_tagedFields.Contains(field))
                {
                    writer.Write("<table><td bgcolor=red>" + field.GetValue(configDataClass[key]) +
                                 "</td></table>" + "|");
                }
                else
                {
                    writer.Write(
                        field.GetValue(configDataClass[key]) + "|");
                }
            }
        }
    }

    private static bool PrintDeletedData<T>(FieldInfo[] fields, StreamWriter writer,
        IDictionary<int, T> oldConfigDataClass,
        IDictionary<int, T> newConfigDataClass)
    {
        bool equalFlag = true;
        bool existFlag = false;
        //打印被删除的记录----------------------------------------------------------------
        writer.WriteLine("##删除记录");
        writer.WriteLine();
        writer.Write("|");
        foreach (FieldInfo field in fields) //打印列名
        {
            if (field.Name != "Data")
            {
                writer.Write("  " + field.Name + "  |");
            }
        }

        writer.WriteLine();

        writer.Write("|");
        foreach (FieldInfo field in fields) //限定表格格式
        {
            if (field.Name != "Data")
            {
                writer.Write("---|");
            }
        }

        writer.WriteLine();

        foreach (var kvp in oldConfigDataClass)
        {
            if (!newConfigDataClass.ContainsKey(kvp.Key))
            {
                equalFlag = false;
                existFlag = true;
                PrintOneToFile(fields, writer, oldConfigDataClass, kvp.Key);
                writer.WriteLine();
            }
        }

        if (equalFlag == true)
        {
            writer.WriteLine("<font color=red>此处无记录</font>");
        }

        writer.WriteLine();
        return existFlag;
    }

    private static bool PrintAddedData<T>(FieldInfo[] fields, StreamWriter writer,
        IDictionary<int, T> oldConfigDataClass,
        IDictionary<int, T> newConfigDataClass)
    {
        bool equalFlag = true;
        bool existFlag = false;
        //打印增加的记录----------------------------------------------------------------
        writer.WriteLine("##增加记录");
        writer.WriteLine();
        writer.Write("|");
        foreach (FieldInfo field in fields) //打印列名
        {
            if (field.Name != "Data")
            {
                writer.Write("  " + field.Name + "  |");
            }
        }

        writer.WriteLine();

        writer.Write("|");
        foreach (FieldInfo field in fields) //限定表格格式
        {
            if (field.Name != "Data")
            {
                writer.Write("---|");
            }
        }

        writer.WriteLine();


        foreach (var kvp in newConfigDataClass)
        {
            if (!oldConfigDataClass.ContainsKey(kvp.Key))
            {
                equalFlag = false;
                existFlag = true;
                PrintOneToFile(fields, writer, newConfigDataClass, kvp.Key);
                writer.WriteLine();
            }
        }

        if (equalFlag == true)
        {
            writer.WriteLine("<font color=red>此处无记录</font>");
        }

        writer.WriteLine();
        return existFlag;
    }

    private static bool PrintChangedData<T>(FieldInfo[] fields, StreamWriter writer,
        IDictionary<int, T> oldConfigDataClass,
        IDictionary<int, T> newConfigDataClass)
    {
        bool equalFlag = true; //是否全部相等
        bool existFlag = false;
        bool thisEqualFlag = true; //单条记录是否相等
        //---------------------------------------------------------------------------------
        //打印修改前后的记录-----------------------------------------------------------------------
        writer.WriteLine("##修改记录(相邻两条上面为修改前，下面为修改后)");
        writer.WriteLine();
        writer.Write("|");
        foreach (FieldInfo field in fields) //打印列名
        {
            if (field.Name != "Data")
            {
                writer.Write("  " + field.Name + "  |");
            }
        }

        writer.WriteLine();

        writer.Write("|");
        foreach (FieldInfo field in fields) //限定表格格式
        {
            if (field.Name != "Data")
            {
                writer.Write("---|");
            }
        }

        writer.WriteLine();


        foreach (var kvp in newConfigDataClass)
        {
            if (oldConfigDataClass.ContainsKey(kvp.Key))
            {
                foreach (FieldInfo field in fields)
                {
                    if (field.Name != "Data")
                    {
                        if (field.FieldType.Name == "ConfigIdCount[]")
                        {
                            if (!ConfigIdCountListCompare(
                                field.GetValue(newConfigDataClass[kvp.Key]) as ConfigIdCount[],
                                field.GetValue(oldConfigDataClass[kvp.Key]) as ConfigIdCount[]))
                            {
                                equalFlag = false;
                                existFlag = true;
                                thisEqualFlag = false;
                                _tagedFields.Add(field);
                            }
                        }
                        else if (field.FieldType.Name == "ConfigProbCount[]")
                        {
                            if (!ConfigProbCountListCompare(
                                field.GetValue(newConfigDataClass[kvp.Key]) as ConfigProbCount[],
                                field.GetValue(oldConfigDataClass[kvp.Key]) as ConfigProbCount[]))
                            {
                                equalFlag = false;
                                existFlag = true;
                                thisEqualFlag = false;
                                _tagedFields.Add(field);
                            }
                        }
                        else if (field.FieldType.Name == "Int32[]")
                        {
                            if (!IntListCompare(
                                field.GetValue(newConfigDataClass[kvp.Key]) as int[],
                                field.GetValue(oldConfigDataClass[kvp.Key]) as int[]))
                            {
                                equalFlag = false;
                                existFlag = true;
                                thisEqualFlag = false;
                                _tagedFields.Add(field);
                            }
                        }
                        else if (field.FieldType.Name == "Single[]")
                        {
                            if (!FloatListCompare(
                                field.GetValue(newConfigDataClass[kvp.Key]) as float[],
                                field.GetValue(oldConfigDataClass[kvp.Key]) as float[]))
                            {
                                equalFlag = false;
                                existFlag = true;
                                thisEqualFlag = false;
                                _tagedFields.Add(field);
                            }
                        }
                        else if (field.FieldType.Name == "ConfigIdCount")
                        {
                            if (!ConfigIdCountCompare(field.GetValue(newConfigDataClass[kvp.Key]) as ConfigIdCount,
                                field.GetValue(oldConfigDataClass[kvp.Key]) as ConfigIdCount))
                            {
                                equalFlag = false;
                                existFlag = true;
                                thisEqualFlag = false;
                                _tagedFields.Add(field);
                            }
                        }
                        else if (!field.GetValue(newConfigDataClass[kvp.Key])
                            .Equals(field.GetValue(oldConfigDataClass[kvp.Key])))
                        {
                            equalFlag = false;
                            existFlag = true;
                            thisEqualFlag = false;
                            _tagedFields.Add(field);
                        }
                    }
                }

                if (!thisEqualFlag)
                {
                    PrintOneToFile(fields, writer, oldConfigDataClass, kvp.Key); //打印修改前记录
                    writer.WriteLine();
                    PrintOneToFile(fields, writer, newConfigDataClass, kvp.Key); //打印修改后记录
                    writer.WriteLine();
                    writer.WriteLine("||");
                    _tagedFields.Clear();
                }

                thisEqualFlag = true;
            }
        }

        if (equalFlag == true)
        {
            writer.WriteLine("<font color=red>此处无记录</font>");
        }

        writer.WriteLine();
        return existFlag;
    }
    

    public static StringBuilder GetIntList(int[] ints)
    {
        StringBuilder str = new StringBuilder("");
        foreach (var VARIABLE in ints)
        {
            str = str.Append(VARIABLE);
            str.Append("，");
        }

        if (ints.Length > 0)
        {
            str.Remove(str.Length - 1, 1);
        }

        return str;
    }

    public static StringBuilder GetConfigIdCountList(ConfigIdCount[] configIdCounts)
    {
        StringBuilder str = new StringBuilder("");
        foreach (var VARIABLE in configIdCounts)
        {
            str = str.Append(VARIABLE.Id);
            str.Append(";");
            str = str.Append(VARIABLE.Count);
            str.Append("&#124;");
        }

        if (str.Length > 0)
        {
            str.Remove(str.Length - 6, 6);
        }

        return str;
    }

    public static StringBuilder GetConfigProbCountList(ConfigProbCount[] configProbCounts)
    {
        StringBuilder str = new StringBuilder("");
        foreach (var VARIABLE in configProbCounts)
        {
            str = str.Append(VARIABLE.Id);
            str.Append(";");
            str = str.Append(VARIABLE.Weight);
            str.Append(";");
            str = str.Append(VARIABLE.Count);
            str.Append("&#124;");
        }

        if (str.Length > 0)
        {
            str.Remove(str.Length - 6, 6);
        }

        return str;
    }

    public static StringBuilder GetFloatList(float[] floats)
    {
        StringBuilder str = new StringBuilder("");
        foreach (var VARIABLE in floats)
        {
            str = str.Append(VARIABLE);
            str.Append("，");
        }

        if (floats.Length > 0)
        {
            str.Remove(str.Length - 1, 1);
        }

        return str;
    }

    public static string GetConfigIdCount(ConfigIdCount configIdCount)
    {
        if (configIdCount == null)
        {
            return "";
        }

        var str = configIdCount.Id.ToString() + ";" + configIdCount.Count.ToString();
        return str;
    }

    public static bool ConfigIdCountListCompare(ConfigIdCount[] configIdCountA, ConfigIdCount[] configIdCountB)
    {
        if (configIdCountA.Length != configIdCountB.Length)
        {
            return false;
        }

        for (int i = 0; i < configIdCountA.Length; i++)
        {
            if (configIdCountA[i].Count != configIdCountB[i].Count || configIdCountA[i].Id != configIdCountB[i].Id)
            {
                return false;
            }
        }

        return true;
    }

    public static bool ConfigProbCountListCompare(ConfigProbCount[] configProbCountA,
        ConfigProbCount[] configProbCountB)
    {
        if (configProbCountA.Length != configProbCountB.Length)
        {
            return false;
        }

        for (int i = 0; i < configProbCountA.Length; i++)
        {
            if (configProbCountA[i].Count != configProbCountB[i].Count ||
                configProbCountA[i].Id != configProbCountB[i].Id ||
                configProbCountA[i].Weight != configProbCountB[i].Weight)
            {
                return false;
            }
        }

        return true;
    }

    public static bool IntListCompare(int[] intsA, int[] intsB)
    {
        if (intsA.Length != intsB.Length)
        {
            return false;
        }

        for (int i = 0; i < intsA.Length; i++)
        {
            if (intsA[i] != intsB[i])
            {
                return false;
            }
        }

        return true;
    }

    public static bool FloatListCompare(float[] floatsA, float[] floatsB)
    {
        if (floatsA.Length != floatsB.Length)
        {
            return false;
        }

        for (int i = 0; i < floatsA.Length; i++)
        {
            if (!floatsA[i].Equals(floatsB[i]))
            {
                return false;
            }
        }

        return true;
    }

    public static bool ConfigIdCountCompare(ConfigIdCount configIdCountA, ConfigIdCount configIdCountB)
    {
        if (configIdCountA == null && configIdCountB == null)
        {
            return true;
        }
        else if (configIdCountA == null || configIdCountB == null)
        {
            return false;
        }
        else if (configIdCountA.Count == configIdCountB.Count && configIdCountA.Id == configIdCountB.Id)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}