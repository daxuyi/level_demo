using System.Collections.Generic;
using Common;

namespace Config
{

    public class Configs
    {
        public static void Init()
        {
            ConfigLoader.Add("dailytrade", ConfDailyTrade.Create);
            ConfigLoader.Add("elementproduct", ConfELementProduct.Create);
            ConfigLoader.Add("element", ConfElement.Create);
            ConfigLoader.Add("elementjar", ConfElementJar.Create);
            ConfigLoader.Add("elementbox", ConfElementBox.Create);
            ConfigLoader.Add("elementup", ConfElementUp.Create);
            ConfigLoader.Add("elementchain", ConfELementChain.Create);
            ConfigLoader.Add("eventparameter", ConfEventParameter.Create);
            ConfigLoader.Add("event", ConfEvent.Create);
            ConfigLoader.Add("fog", ConfFog.Create);
            ConfigLoader.Add("guide", ConfGuide.Create);
            ConfigLoader.Add("talk", ConfTalk.Create);
            ConfigLoader.Add("levelmap", ConfLevelMap.Create);
            ConfigLoader.Add("level", ConfLevel.Create);
            ConfigLoader.Add("npc", ConfNpc.Create);
            ConfigLoader.Add("npcbook", ConfNpcBook.Create);
            ConfigLoader.Add("quest", ConfQuest.Create);
            ConfigLoader.Add("rulecombo", ConfRuleCombo.Create);
            ConfigLoader.Add("ruleloadingtips", ConfRuleLoadingTips.Create);
            ConfigLoader.Add("ruleparameter", ConfRuleParameter.Create);
            ConfigLoader.Add("ruledailybox", ConfRuleDailyBox.Create);
            ConfigLoader.Add("rulelevelbox", ConfRuleLevelBox.Create);
            ConfigLoader.Add("rulefeature", ConfRuleFeature.Create);
            ConfigLoader.Add("rulelandpoints", ConfRuleLandPoints.Create);
            ConfigLoader.Add("shop", ConfShop.Create);
            ConfigLoader.Add("shoptrade", ConfShopTrade.Create);
            ConfigLoader.Add("shopcharge", ConfShopCharge.Create);
        }
    }


	public class ConfDailyTrade
	{
    	public static IDictionary<int, ConfDailyTrade> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfDailyTrade>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfDailyTrade(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfDailyTrade>();
			}
		}

		public static ConfDailyTrade Get(int id)
		{
			ConfDailyTrade conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfDailyTrade(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			UnlockPower = loader.GetIntArray("UnlockPower");
			RandomPro = loader.GetInt("RandomPro");
			ElementOffer = loader.GetInt("ElementOffer");
			ElementOfferNum = loader.GetIntArray("ElementOfferNum");
			ElementGain = loader.GetIdCountArray("ElementGain");
			ElementGainNum = loader.GetInt("ElementGainNum");
			ElementGainPackage = loader.GetInt("ElementGainPackage");
			ElementBonusGain = loader.GetProbCountArray("ElementBonusGain");
			ElementBonusGainPro = loader.GetInt("ElementBonusGainPro");
			ElementBonusGainPackage = loader.GetInt("ElementBonusGainPackage");
		}

		public readonly int ID;
		public readonly int[] UnlockPower;
		public readonly int RandomPro;
		public readonly int ElementOffer;
		public readonly int[] ElementOfferNum;
		public readonly ConfigIdCount[] ElementGain;
		public readonly int ElementGainNum;
		public readonly int ElementGainPackage;
		public readonly ConfigProbCount[] ElementBonusGain;
		public readonly int ElementBonusGainPro;
		public readonly int ElementBonusGainPackage;
	}


	public class ConfElement
	{
    	public static IDictionary<int, ConfElement> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfElement>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfElement(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfElement>();
			}
		}

		public static ConfElement Get(int id)
		{
			ConfElement conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfElement(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			Type = loader.GetEnum<ElementType>("Type");
			EventID = loader.GetIntArray("EventID");
			isEventRemain = loader.GetBool("isEventRemain");
			Chain = loader.GetInt("Chain");
			Name = loader.GetString("Name");
			Des = loader.GetString("Des");
			LevelID = loader.GetInt("LevelID");
			Level = loader.GetString("Level");
			QualityID = loader.GetInt("QualityID");
			Quality = loader.GetString("Quality");
			Width = loader.GetInt("Width");
			Height = loader.GetInt("Height");
			Param = loader.GetInt("Param");
			GemPurchase = loader.GetInt("GemPurchase");
			CoinSale = loader.GetInt("CoinSale");
			CoinSalePrompt = loader.GetBool("CoinSalePrompt");
			UsePrompt = loader.GetBool("UsePrompt");
			TipArrow = loader.GetBool("TipArrow");
			TipType = loader.GetInt("TipType");
			AssetBundle = loader.GetString("AssetBundle");
			Icon = loader.GetString("Icon");
			OwnEffect = loader.GetString("OwnEffect");
			AppearEffect = loader.GetString("AppearEffect");
			MergeSound = loader.GetString("MergeSound");
			AppearSound = loader.GetString("AppearSound");
			EffectDisplay = loader.GetBool("EffectDisplay");
			isMerge = loader.GetBool("isMerge");
			MergeProduct = loader.GetIdCountArray("MergeProduct");
			MergeByProductProb = loader.GetProbCountArray("MergeByProductProb");
			MergeLevelProductProb = loader.GetProbCountArray("MergeLevelProductProb");
			MergeJar = loader.GetInt("MergeJar");
			MergePoint = loader.GetInt("MergePoint");
			isMove = loader.GetBool("isMove");
			isCollect = loader.GetBool("isCollect");
			CollectProduct = loader.GetInt("CollectProduct");
			CollectTimeMin = loader.GetInt("CollectTimeMin");
			CollectTimeMax = loader.GetInt("CollectTimeMax");
			CollectProductLimit = loader.GetInt("CollectProductLimit");
			CollectNpcLimit = loader.GetInt("CollectNpcLimit");
			CollectNeedValue = loader.GetInt("CollectNeedValue");
			CollectChangeCount = loader.GetInt("CollectChangeCount");
			CollectChangeRate = loader.GetFloat("CollectChangeRate");
			CollectChangeID = loader.GetInt("CollectChangeID");
			isAttackNPC = loader.GetBool("isAttackNPC");
			isAttackEnemy = loader.GetBool("isAttackEnemy");
			AttackEffect = loader.GetString("AttackEffect");
			AttackSound = loader.GetString("AttackSound");
			AttackProduct = loader.GetInt("AttackProduct");
			AttackProductNumMin = loader.GetInt("AttackProductNumMin");
			AttackProductNumMax = loader.GetInt("AttackProductNumMax");
			AttackNeedValue = loader.GetInt("AttackNeedValue");
			isClick = loader.GetBool("isClick");
			ClickableEffect = loader.GetString("ClickableEffect");
			ClickEffect = loader.GetString("ClickEffect");
			ClickSound = loader.GetString("ClickSound");
			ClickProduct = loader.GetInt("ClickProduct");
			ClickProductNumMin = loader.GetInt("ClickProductNumMin");
			ClickProductNumMax = loader.GetInt("ClickProductNumMax");
			ClickProductList = loader.GetIdCountArray("ClickProductList");
			ClickByProduct = loader.GetInt("ClickByProduct");
			ClickByProductCountMin = loader.GetInt("ClickByProductCountMin");
			ClickByProductCountMax = loader.GetInt("ClickByProductCountMax");
			ClickCountMin = loader.GetInt("ClickCountMin");
			ClickCountMax = loader.GetInt("ClickCountMax");
			ClickResumeTimeMin = loader.GetInt("ClickResumeTimeMin");
			ClickResumeTimeMax = loader.GetInt("ClickResumeTimeMax");
			ClickChangeType = loader.GetEnum<ClickChangeType>("ClickChangeType");
			ClickChangeCountMin = loader.GetInt("ClickChangeCountMin");
			ClickChangeCountMax = loader.GetInt("ClickChangeCountMax");
			ClickChangeRate = loader.GetFloat("ClickChangeRate");
			ClickChangeID = loader.GetInt("ClickChangeID");
			isAutoProduct = loader.GetBool("isAutoProduct");
			isAutoOnDead = loader.GetBool("isAutoOnDead");
			AutoProductEffect = loader.GetString("AutoProductEffect");
			AutoProductSound = loader.GetString("AutoProductSound");
			AutoProduct = loader.GetInt("AutoProduct");
			AutoProductNumMin = loader.GetInt("AutoProductNumMin");
			AutoProductNumMax = loader.GetInt("AutoProductNumMax");
			AutoProductLimit = loader.GetInt("AutoProductLimit");
			AutoProductRange = loader.GetInt("AutoProductRange");
			AutoProductTimeMin = loader.GetInt("AutoProductTimeMin");
			AutoProductTimeMax = loader.GetInt("AutoProductTimeMax");
			AutoProductType = loader.GetEnum<AutoProductType>("AutoProductType");
			AutoProductTotalChangeNum = loader.GetInt("AutoProductTotalChangeNum");
			AutoProductChangeRate = loader.GetFloat("AutoProductChangeRate");
			AutoProductNextID = loader.GetInt("AutoProductNextID");
			isAutoChange = loader.GetBool("isAutoChange");
			AutoChangeNextProbID = loader.GetIdCountArray("AutoChangeNextProbID");
			AutoChangeTimeMin = loader.GetInt("AutoChangeTimeMin");
			AutoChangeTimeMax = loader.GetInt("AutoChangeTimeMax");
		}

		public readonly int ID;
		public readonly ElementType Type;
		public readonly int[] EventID;
		public readonly bool isEventRemain;
		public readonly int Chain;
		public readonly string Name;
		public readonly string Des;
		public readonly int LevelID;
		public readonly string Level;
		public readonly int QualityID;
		public readonly string Quality;
		public readonly int Width;
		public readonly int Height;
		public readonly int Param;
		public readonly int GemPurchase;
		public readonly int CoinSale;
		public readonly bool CoinSalePrompt;
		public readonly bool UsePrompt;
		public readonly bool TipArrow;
		public readonly int TipType;
		public readonly string AssetBundle;
		public readonly string Icon;
		public readonly string OwnEffect;
		public readonly string AppearEffect;
		public readonly string MergeSound;
		public readonly string AppearSound;
		public readonly bool EffectDisplay;
		public readonly bool isMerge;
		public readonly ConfigIdCount[] MergeProduct;
		public readonly ConfigProbCount[] MergeByProductProb;
		public readonly ConfigProbCount[] MergeLevelProductProb;
		public readonly int MergeJar;
		public readonly int MergePoint;
		public readonly bool isMove;
		public readonly bool isCollect;
		public readonly int CollectProduct;
		public readonly int CollectTimeMin;
		public readonly int CollectTimeMax;
		public readonly int CollectProductLimit;
		public readonly int CollectNpcLimit;
		public readonly int CollectNeedValue;
		public readonly int CollectChangeCount;
		public readonly float CollectChangeRate;
		public readonly int CollectChangeID;
		public readonly bool isAttackNPC;
		public readonly bool isAttackEnemy;
		public readonly string AttackEffect;
		public readonly string AttackSound;
		public readonly int AttackProduct;
		public readonly int AttackProductNumMin;
		public readonly int AttackProductNumMax;
		public readonly int AttackNeedValue;
		public readonly bool isClick;
		public readonly string ClickableEffect;
		public readonly string ClickEffect;
		public readonly string ClickSound;
		public readonly int ClickProduct;
		public readonly int ClickProductNumMin;
		public readonly int ClickProductNumMax;
		public readonly ConfigIdCount[] ClickProductList;
		public readonly int ClickByProduct;
		public readonly int ClickByProductCountMin;
		public readonly int ClickByProductCountMax;
		public readonly int ClickCountMin;
		public readonly int ClickCountMax;
		public readonly int ClickResumeTimeMin;
		public readonly int ClickResumeTimeMax;
		public readonly ClickChangeType ClickChangeType;
		public readonly int ClickChangeCountMin;
		public readonly int ClickChangeCountMax;
		public readonly float ClickChangeRate;
		public readonly int ClickChangeID;
		public readonly bool isAutoProduct;
		public readonly bool isAutoOnDead;
		public readonly string AutoProductEffect;
		public readonly string AutoProductSound;
		public readonly int AutoProduct;
		public readonly int AutoProductNumMin;
		public readonly int AutoProductNumMax;
		public readonly int AutoProductLimit;
		public readonly int AutoProductRange;
		public readonly int AutoProductTimeMin;
		public readonly int AutoProductTimeMax;
		public readonly AutoProductType AutoProductType;
		public readonly int AutoProductTotalChangeNum;
		public readonly float AutoProductChangeRate;
		public readonly int AutoProductNextID;
		public readonly bool isAutoChange;
		public readonly ConfigIdCount[] AutoChangeNextProbID;
		public readonly int AutoChangeTimeMin;
		public readonly int AutoChangeTimeMax;
	}


	public class ConfElementBox
	{
    	public static IDictionary<int, ConfElementBox> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfElementBox>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfElementBox(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfElementBox>();
			}
		}

		public static ConfElementBox Get(int id)
		{
			ConfElementBox conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfElementBox(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			ElementID = loader.GetInt("ElementID");
			Type = loader.GetEnum<BoxType>("Type");
			ChargeId = loader.GetInt("ChargeId");
			Price = loader.GetInt("Price");
			Time = loader.GetInt("Time");
			MergeTo = loader.GetInt("MergeTo");
			TurnTo = loader.GetInt("TurnTo");
			isOrb = loader.GetBool("isOrb");
			OrbNum = loader.GetInt("OrbNum");
			Product = loader.GetInt("Product");
			ProductNumMin = loader.GetInt("ProductNumMin");
			ProductNumMax = loader.GetInt("ProductNumMax");
			ProductList = loader.GetIdCountArray("ProductList");
			ByProduct1 = loader.GetInt("ByProduct1");
			ByProductCountMin1 = loader.GetInt("ByProductCountMin1");
			ByProductCountMax1 = loader.GetInt("ByProductCountMax1");
			ByProduct2 = loader.GetInt("ByProduct2");
			ByProductCountMin2 = loader.GetInt("ByProductCountMin2");
			ByProductCountMax2 = loader.GetInt("ByProductCountMax2");
		}

		public readonly int ID;
		public readonly int ElementID;
		public readonly BoxType Type;
		public readonly int ChargeId;
		public readonly int Price;
		public readonly int Time;
		public readonly int MergeTo;
		public readonly int TurnTo;
		public readonly bool isOrb;
		public readonly int OrbNum;
		public readonly int Product;
		public readonly int ProductNumMin;
		public readonly int ProductNumMax;
		public readonly ConfigIdCount[] ProductList;
		public readonly int ByProduct1;
		public readonly int ByProductCountMin1;
		public readonly int ByProductCountMax1;
		public readonly int ByProduct2;
		public readonly int ByProductCountMin2;
		public readonly int ByProductCountMax2;
	}


	public class ConfELementChain
	{
    	public static IDictionary<int, ConfELementChain> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfELementChain>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfELementChain(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfELementChain>();
			}
		}

		public static ConfELementChain Get(int id)
		{
			ConfELementChain conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfELementChain(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			Name = loader.GetString("Name");
			Item = loader.GetIntArray("Item");
		}

		public readonly int ID;
		public readonly string Name;
		public readonly int[] Item;
	}


	public class ConfElementJar
	{
    	public static IDictionary<int, ConfElementJar> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfElementJar>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfElementJar(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfElementJar>();
			}
		}

		public static ConfElementJar Get(int id)
		{
			ConfElementJar conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfElementJar(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			ElementID = loader.GetInt("ElementID");
			ContainElementID = loader.GetInt("ContainElementID");
			ContainElementScale = loader.GetFloat("ContainElementScale");
			CampUse = loader.GetBool("CampUse");
			CampPriceFirst = loader.GetInt("CampPriceFirst");
			CampTimeFirst = loader.GetInt("CampTimeFirst");
			CampPrice = loader.GetInt("CampPrice");
			CampTime = loader.GetInt("CampTime");
			CampProb = loader.GetFloat("CampProb");
			EventUse = loader.GetBool("EventUse");
			EventPrice = loader.GetInt("EventPrice");
			EventTime = loader.GetInt("EventTime");
			EventProb = loader.GetFloat("EventProb");
		}

		public readonly int ID;
		public readonly int ElementID;
		public readonly int ContainElementID;
		public readonly float ContainElementScale;
		public readonly bool CampUse;
		public readonly int CampPriceFirst;
		public readonly int CampTimeFirst;
		public readonly int CampPrice;
		public readonly int CampTime;
		public readonly float CampProb;
		public readonly bool EventUse;
		public readonly int EventPrice;
		public readonly int EventTime;
		public readonly float EventProb;
	}


	public class ConfELementProduct
	{
    	public static IDictionary<int, ConfELementProduct> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfELementProduct>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfELementProduct(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfELementProduct>();
			}
		}

		public static ConfELementProduct Get(int id)
		{
			ConfELementProduct conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfELementProduct(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			Type = loader.GetEnum<ElementProductType>("Type");
			DefaultProduct = loader.GetInt("DefaultProduct");
			Product1 = loader.GetInt("Product1");
			ProductChance1 = loader.GetFloat("ProductChance1");
			Product2 = loader.GetInt("Product2");
			ProductChance2 = loader.GetFloat("ProductChance2");
			Product3 = loader.GetInt("Product3");
			ProductChance3 = loader.GetFloat("ProductChance3");
			Product4 = loader.GetInt("Product4");
			ProductChance4 = loader.GetFloat("ProductChance4");
			Product5 = loader.GetInt("Product5");
			ProductChance5 = loader.GetFloat("ProductChance5");
		}

		public readonly int ID;
		public readonly ElementProductType Type;
		public readonly int DefaultProduct;
		public readonly int Product1;
		public readonly float ProductChance1;
		public readonly int Product2;
		public readonly float ProductChance2;
		public readonly int Product3;
		public readonly float ProductChance3;
		public readonly int Product4;
		public readonly float ProductChance4;
		public readonly int Product5;
		public readonly float ProductChance5;
	}


	public class ConfElementUp
	{
    	public static IDictionary<int, ConfElementUp> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfElementUp>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfElementUp(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfElementUp>();
			}
		}

		public static ConfElementUp Get(int id)
		{
			ConfElementUp conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfElementUp(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			Type = loader.GetEnum<ElementUpType>("Type");
			Cost = loader.GetInt("Cost");
			Goal = loader.GetInt("Goal");
			RewardType = loader.GetEnum<RewardType>("RewardType");
			Reward = loader.GetIdCountArray("Reward");
			Effect1 = loader.GetString("Effect1");
			Effect2 = loader.GetString("Effect2");
		}

		public readonly int ID;
		public readonly ElementUpType Type;
		public readonly int Cost;
		public readonly int Goal;
		public readonly RewardType RewardType;
		public readonly ConfigIdCount[] Reward;
		public readonly string Effect1;
		public readonly string Effect2;
	}


	public class ConfEvent
	{
    	public static IDictionary<int, ConfEvent> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfEvent>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfEvent(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfEvent>();
			}
		}

		public static ConfEvent Get(int id)
		{
			ConfEvent conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfEvent(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			isOpen = loader.GetBool("isOpen");
			Map = loader.GetString("Map");
			Scale = loader.GetFloat("Scale");
			Name = loader.GetString("Name");
			AdvertiseSpine = loader.GetString("AdvertiseSpine");
			UIResource = loader.GetString("UIResource");
			UIPrefab = loader.GetString("UIPrefab");
			UIFont1 = loader.GetString("UIFont1");
			UIFont2 = loader.GetString("UIFont2");
			Quest1 = loader.GetInt("Quest1");
			Quest2 = loader.GetInt("Quest2");
			Quest3 = loader.GetInt("Quest3");
			QuestReward = loader.GetIdCountArray("QuestReward");
			QuestNum = loader.GetInt("QuestNum");
			RewardPoint = loader.GetIntArray("RewardPoint");
			RewardElement = loader.GetIntArray("RewardElement");
			ChestPointReward = loader.GetInt("ChestPointReward");
			Chest1 = loader.GetInt("Chest1");
			ChestPossibleReward1 = loader.GetIntArray("ChestPossibleReward1");
			Chest2 = loader.GetInt("Chest2");
			ChestPossibleReward2 = loader.GetIntArray("ChestPossibleReward2");
			Chest3 = loader.GetInt("Chest3");
			ChestPossibleReward3 = loader.GetIntArray("ChestPossibleReward3");
			Chest4 = loader.GetInt("Chest4");
			ChestPossibleReward4 = loader.GetIntArray("ChestPossibleReward4");
			ChargeBundle = loader.GetInt("ChargeBundle");
		}

		public readonly int ID;
		public readonly bool isOpen;
		public readonly string Map;
		public readonly float Scale;
		public readonly string Name;
		public readonly string AdvertiseSpine;
		public readonly string UIResource;
		public readonly string UIPrefab;
		public readonly string UIFont1;
		public readonly string UIFont2;
		public readonly int Quest1;
		public readonly int Quest2;
		public readonly int Quest3;
		public readonly ConfigIdCount[] QuestReward;
		public readonly int QuestNum;
		public readonly int[] RewardPoint;
		public readonly int[] RewardElement;
		public readonly int ChestPointReward;
		public readonly int Chest1;
		public readonly int[] ChestPossibleReward1;
		public readonly int Chest2;
		public readonly int[] ChestPossibleReward2;
		public readonly int Chest3;
		public readonly int[] ChestPossibleReward3;
		public readonly int Chest4;
		public readonly int[] ChestPossibleReward4;
		public readonly int ChargeBundle;
	}


	public class ConfEventParameter
	{
    	public static IDictionary<int, ConfEventParameter> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfEventParameter>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfEventParameter(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfEventParameter>();
			}
		}

		public static ConfEventParameter Get(int id)
		{
			ConfEventParameter conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfEventParameter(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			ChestUnlock = loader.GetInt("ChestUnlock");
			ChestResumeTime = loader.GetInt("ChestResumeTime");
			RewardSpinPrice = loader.GetInt("RewardSpinPrice");
			RewardRespinPrice = loader.GetIntArray("RewardRespinPrice");
		}

		public readonly int ID;
		public readonly int ChestUnlock;
		public readonly int ChestResumeTime;
		public readonly int RewardSpinPrice;
		public readonly int[] RewardRespinPrice;
	}


	public class ConfFog
	{
    	public static IDictionary<int, ConfFog> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfFog>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfFog(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfFog>();
			}
		}

		public static ConfFog Get(int id)
		{
			ConfFog conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfFog(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			ShowPosition = loader.GetIntArray("ShowPosition");
			UnlockPower = loader.GetInt("UnlockPower");
			Charge = loader.GetBool("Charge");
			ChargeBundle = loader.GetInt("ChargeBundle");
			ShowNext = loader.GetIntArray("ShowNext");
		}

		public readonly int ID;
		public readonly int[] ShowPosition;
		public readonly int UnlockPower;
		public readonly bool Charge;
		public readonly int ChargeBundle;
		public readonly int[] ShowNext;
	}


	public class ConfGuide
	{
    	public static IDictionary<int, ConfGuide> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfGuide>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfGuide(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfGuide>();
			}
		}

		public static ConfGuide Get(int id)
		{
			ConfGuide conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfGuide(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			GroupId = loader.GetInt("GroupId");
			StepId = loader.GetInt("StepId");
			TalkIcon = loader.GetString("TalkIcon");
			TalkName = loader.GetString("TalkName");
			TalkGroupId = loader.GetInt("TalkGroupId");
			PointText = loader.GetString("PointText");
			PointTextPosition = loader.GetFloatArray("PointTextPosition");
			FocusElementId = loader.GetInt("FocusElementId");
			FocusFogId = loader.GetInt("FocusFogId");
			Quest = loader.GetInt("Quest");
			QuestEffect = loader.GetString("QuestEffect");
			QuestText = loader.GetString("QuestText");
			RestoreGuide = loader.GetInt("RestoreGuide");
		}

		public readonly int ID;
		public readonly int GroupId;
		public readonly int StepId;
		public readonly string TalkIcon;
		public readonly string TalkName;
		public readonly int TalkGroupId;
		public readonly string PointText;
		public readonly float[] PointTextPosition;
		public readonly int FocusElementId;
		public readonly int FocusFogId;
		public readonly int Quest;
		public readonly string QuestEffect;
		public readonly string QuestText;
		public readonly int RestoreGuide;
	}


	public class ConfLevel
	{
    	public static IDictionary<int, ConfLevel> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfLevel>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfLevel(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfLevel>();
			}
		}

		public static ConfLevel Get(int id)
		{
			ConfLevel conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfLevel(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			LevelType = loader.GetEnum<LevelType>("LevelType");
			Map = loader.GetString("Map");
			Name = loader.GetString("Name");
			ChallengeName = loader.GetString("ChallengeName");
			Icon = loader.GetString("Icon");
			IconScale = loader.GetFloat("IconScale");
			IconPosition = loader.GetInt("IconPosition");
			PrefabUse = loader.GetString("PrefabUse");
			PrefabInOrder = loader.GetInt("PrefabInOrder");
			Music = loader.GetString("Music");
			IsExtraProduct = loader.GetBool("IsExtraProduct");
			CommonPower = loader.GetInt("CommonPower");
			Challenge = loader.GetBool("Challenge");
			ChallengePower = loader.GetIntArray("ChallengePower");
			RewardBoxShow = loader.GetInt("RewardBoxShow");
			RewardElementShow = loader.GetIntArray("RewardElementShow");
			Scale = loader.GetFloat("Scale");
			Unlock = loader.GetIntArray("Unlock");
			NextID = loader.GetInt("NextID");
			FinishMission = loader.GetInt("FinishMission");
			CommonMission1 = loader.GetInt("CommonMission1");
			CommonMission2 = loader.GetInt("CommonMission2");
			CommonMission3 = loader.GetInt("CommonMission3");
			CompleteElement = loader.GetIdCountArray("CompleteElement");
			CompleteElementCount = loader.GetIntArray("CompleteElementCount");
			CoreReward = loader.GetProbCountArray("CoreReward");
			FreeReward1 = loader.GetProbCountArray("FreeReward1");
			FreeRewardNum1 = loader.GetInt("FreeRewardNum1");
			FreeReward2 = loader.GetProbCountArray("FreeReward2");
			FreeRewardNum2 = loader.GetInt("FreeRewardNum2");
			CommonPayReward = loader.GetProbCountArray("CommonPayReward");
			CommonPayRewardNum = loader.GetInt("CommonPayRewardNum");
			LandPayRewardNum = loader.GetInt("LandPayRewardNum");
			CommonBoxRewardProb = loader.GetIdCountArray("CommonBoxRewardProb");
			CommonWinReward = loader.GetIdCountArray("CommonWinReward");
			ChallengeTime = loader.GetIntArray("ChallengeTime");
			ChallengeReward = loader.GetIdCountArray("ChallengeReward");
		}

		public readonly int ID;
		public readonly LevelType LevelType;
		public readonly string Map;
		public readonly string Name;
		public readonly string ChallengeName;
		public readonly string Icon;
		public readonly float IconScale;
		public readonly int IconPosition;
		public readonly string PrefabUse;
		public readonly int PrefabInOrder;
		public readonly string Music;
		public readonly bool IsExtraProduct;
		public readonly int CommonPower;
		public readonly bool Challenge;
		public readonly int[] ChallengePower;
		public readonly int RewardBoxShow;
		public readonly int[] RewardElementShow;
		public readonly float Scale;
		public readonly int[] Unlock;
		public readonly int NextID;
		public readonly int FinishMission;
		public readonly int CommonMission1;
		public readonly int CommonMission2;
		public readonly int CommonMission3;
		public readonly ConfigIdCount[] CompleteElement;
		public readonly int[] CompleteElementCount;
		public readonly ConfigProbCount[] CoreReward;
		public readonly ConfigProbCount[] FreeReward1;
		public readonly int FreeRewardNum1;
		public readonly ConfigProbCount[] FreeReward2;
		public readonly int FreeRewardNum2;
		public readonly ConfigProbCount[] CommonPayReward;
		public readonly int CommonPayRewardNum;
		public readonly int LandPayRewardNum;
		public readonly ConfigIdCount[] CommonBoxRewardProb;
		public readonly ConfigIdCount[] CommonWinReward;
		public readonly int[] ChallengeTime;
		public readonly ConfigIdCount[] ChallengeReward;
	}


	public class ConfLevelMap
	{
    	public static IDictionary<int, ConfLevelMap> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfLevelMap>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfLevelMap(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfLevelMap>();
			}
		}

		public static ConfLevelMap Get(int id)
		{
			ConfLevelMap conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfLevelMap(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			Prefab = loader.GetString("Prefab");
			PositionPort = loader.GetFloatArray("PositionPort");
			PositionLand = loader.GetFloatArray("PositionLand");
		}

		public readonly int ID;
		public readonly string Prefab;
		public readonly float[] PositionPort;
		public readonly float[] PositionLand;
	}


	public class ConfNpc
	{
    	public static IDictionary<int, ConfNpc> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfNpc>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfNpc(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfNpc>();
			}
		}

		public static ConfNpc Get(int id)
		{
			ConfNpc conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfNpc(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			Type = loader.GetEnum<NpcType>("Type");
			Chain = loader.GetInt("Chain");
			Name = loader.GetString("Name");
			Des = loader.GetString("Des");
			LevelID = loader.GetInt("LevelID");
			Level = loader.GetString("Level");
			QualityID = loader.GetInt("QualityID");
			Quality = loader.GetString("Quality");
			GemPurchase = loader.GetInt("GemPurchase");
			CoinSale = loader.GetInt("CoinSale");
			AssetBundle = loader.GetString("AssetBundle");
			ABUIScale = loader.GetFloat("ABUIScale");
			Effect1 = loader.GetString("Effect1");
			Effect2 = loader.GetString("Effect2");
			Effect3 = loader.GetString("Effect3");
			Power = loader.GetInt("Power");
			Endurance = loader.GetInt("Endurance");
			Attack = loader.GetInt("Attack");
			AttackSpeed = loader.GetFloat("AttackSpeed");
			AttackJudgeScale = loader.GetFloat("AttackJudgeScale");
			AttackRank = loader.GetFloatArray("AttackRank");
			BaseSpeed = loader.GetFloat("BaseSpeed");
			CarrySpeed = loader.GetFloat("CarrySpeed");
			MoveSpeed = loader.GetFloat("MoveSpeed");
			WorkJudgeScale = loader.GetFloat("WorkJudgeScale");
			BuildSpeed = loader.GetFloat("BuildSpeed");
			CollectSpeed = loader.GetFloat("CollectSpeed");
			isMerge = loader.GetBool("isMerge");
			MergeType = loader.GetEnum<MergeType>("MergeType");
			MergeProduct = loader.GetIdCount("MergeProduct");
			MergeJar = loader.GetInt("MergeJar");
			MergePoint = loader.GetInt("MergePoint");
			isAttackNPC = loader.GetBool("isAttackNPC");
			AttackProduct = loader.GetInt("AttackProduct");
			AttackProductNumMin = loader.GetInt("AttackProductNumMin");
			AttackProductNumMax = loader.GetInt("AttackProductNumMax");
			AttackProductList = loader.GetIdCount("AttackProductList");
			AttackNeedValue = loader.GetInt("AttackNeedValue");
		}

		public readonly int ID;
		public readonly NpcType Type;
		public readonly int Chain;
		public readonly string Name;
		public readonly string Des;
		public readonly int LevelID;
		public readonly string Level;
		public readonly int QualityID;
		public readonly string Quality;
		public readonly int GemPurchase;
		public readonly int CoinSale;
		public readonly string AssetBundle;
		public readonly float ABUIScale;
		public readonly string Effect1;
		public readonly string Effect2;
		public readonly string Effect3;
		public readonly int Power;
		public readonly int Endurance;
		public readonly int Attack;
		public readonly float AttackSpeed;
		public readonly float AttackJudgeScale;
		public readonly float[] AttackRank;
		public readonly float BaseSpeed;
		public readonly float CarrySpeed;
		public readonly float MoveSpeed;
		public readonly float WorkJudgeScale;
		public readonly float BuildSpeed;
		public readonly float CollectSpeed;
		public readonly bool isMerge;
		public readonly MergeType MergeType;
		public readonly ConfigIdCount MergeProduct;
		public readonly int MergeJar;
		public readonly int MergePoint;
		public readonly bool isAttackNPC;
		public readonly int AttackProduct;
		public readonly int AttackProductNumMin;
		public readonly int AttackProductNumMax;
		public readonly ConfigIdCount AttackProductList;
		public readonly int AttackNeedValue;
	}


	public class ConfNpcBook
	{
    	public static IDictionary<int, ConfNpcBook> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfNpcBook>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfNpcBook(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfNpcBook>();
			}
		}

		public static ConfNpcBook Get(int id)
		{
			ConfNpcBook conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfNpcBook(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			TabDisplay = loader.GetInt("TabDisplay");
			TabIcon = loader.GetString("TabIcon");
			TabOrder = loader.GetInt("TabOrder");
			Name = loader.GetString("Name");
			Npc = loader.GetIntArray("Npc");
		}

		public readonly int ID;
		public readonly int TabDisplay;
		public readonly string TabIcon;
		public readonly int TabOrder;
		public readonly string Name;
		public readonly int[] Npc;
	}


	public class ConfQuest
	{
    	public static IDictionary<int, ConfQuest> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfQuest>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfQuest(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfQuest>();
			}
		}

		public static ConfQuest Get(int id)
		{
			ConfQuest conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfQuest(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			isStar = loader.GetBool("isStar");
			Name = loader.GetString("Name");
			Des = loader.GetString("Des");
			Icon = loader.GetString("Icon");
			Type = loader.GetEnum<QuestType>("Type");
			CertainID = loader.GetIntArray("CertainID");
			ChainID = loader.GetIntArray("ChainID");
			RequireCount = loader.GetInt("RequireCount");
			Param = loader.GetInt("Param");
			RewardShow = loader.GetInt("RewardShow");
			RewardGainProb = loader.GetIdCountArray("RewardGainProb");
			Skip = loader.GetInt("Skip");
			NextID = loader.GetInt("NextID");
			GuideType = loader.GetEnum<QuestGuideType>("GuideType");
			GuideParam = loader.GetIntArray("GuideParam");
		}

		public readonly int ID;
		public readonly bool isStar;
		public readonly string Name;
		public readonly string Des;
		public readonly string Icon;
		public readonly QuestType Type;
		public readonly int[] CertainID;
		public readonly int[] ChainID;
		public readonly int RequireCount;
		public readonly int Param;
		public readonly int RewardShow;
		public readonly ConfigIdCount[] RewardGainProb;
		public readonly int Skip;
		public readonly int NextID;
		public readonly QuestGuideType GuideType;
		public readonly int[] GuideParam;
	}


	public class ConfRuleCombo
	{
    	public static IDictionary<int, ConfRuleCombo> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfRuleCombo>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfRuleCombo(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfRuleCombo>();
			}
		}

		public static ConfRuleCombo Get(int id)
		{
			ConfRuleCombo conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfRuleCombo(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			ElementID = loader.GetIdCount("ElementID");
		}

		public readonly int ID;
		public readonly ConfigIdCount ElementID;
	}


	public class ConfRuleDailyBox
	{
    	public static IDictionary<int, ConfRuleDailyBox> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfRuleDailyBox>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfRuleDailyBox(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfRuleDailyBox>();
			}
		}

		public static ConfRuleDailyBox Get(int id)
		{
			ConfRuleDailyBox conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfRuleDailyBox(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			BoxID = loader.GetInt("BoxID");
		}

		public readonly int ID;
		public readonly int BoxID;
	}


	public class ConfRuleFeature
	{
    	public static IDictionary<int, ConfRuleFeature> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfRuleFeature>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfRuleFeature(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfRuleFeature>();
			}
		}

		public static ConfRuleFeature Get(int id)
		{
			ConfRuleFeature conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfRuleFeature(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			Name = loader.GetEnum<RuleFeatureName>("Name");
			UnlockPower = loader.GetInt("UnlockPower");
			UnlockFog = loader.GetInt("UnlockFog");
			UnlockGuide = loader.GetIntArray("UnlockGuide");
		}

		public readonly int ID;
		public readonly RuleFeatureName Name;
		public readonly int UnlockPower;
		public readonly int UnlockFog;
		public readonly int[] UnlockGuide;
	}


	public class ConfRuleLandPoints
	{
    	public static IDictionary<int, ConfRuleLandPoints> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfRuleLandPoints>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfRuleLandPoints(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfRuleLandPoints>();
			}
		}

		public static ConfRuleLandPoints Get(int id)
		{
			ConfRuleLandPoints conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfRuleLandPoints(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			HealingLandPoints = loader.GetInt("HealingLandPoints");
			DeadValue = loader.GetIntArray("DeadValue");
		}

		public readonly int ID;
		public readonly int HealingLandPoints;
		public readonly int[] DeadValue;
	}


	public class ConfRuleLevelBox
	{
    	public static IDictionary<int, ConfRuleLevelBox> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfRuleLevelBox>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfRuleLevelBox(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfRuleLevelBox>();
			}
		}

		public static ConfRuleLevelBox Get(int id)
		{
			ConfRuleLevelBox conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfRuleLevelBox(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			UnlockTime = loader.GetInt("UnlockTime");
			UnlockPrice = loader.GetInt("UnlockPrice");
		}

		public readonly int ID;
		public readonly int UnlockTime;
		public readonly int UnlockPrice;
	}


	public class ConfRuleLoadingTips
	{
    	public static IDictionary<int, ConfRuleLoadingTips> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfRuleLoadingTips>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfRuleLoadingTips(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfRuleLoadingTips>();
			}
		}

		public static ConfRuleLoadingTips Get(int id)
		{
			ConfRuleLoadingTips conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfRuleLoadingTips(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			Tips = loader.GetString("Tips");
		}

		public readonly int ID;
		public readonly string Tips;
	}


	public class ConfRuleParameter
	{
    	public static IDictionary<int, ConfRuleParameter> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfRuleParameter>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfRuleParameter(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfRuleParameter>();
			}
		}

		public static ConfRuleParameter Get(int id)
		{
			ConfRuleParameter conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfRuleParameter(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			InitialCoin = loader.GetIntArray("InitialCoin");
			InitialWood = loader.GetIntArray("InitialWood");
			InitialGem = loader.GetIntArray("InitialGem");
			InitialUserPower = loader.GetIntArray("InitialUserPower");
			UserPowerRestore = loader.GetIntArray("UserPowerRestore");
			AdColdTime = loader.GetInt("AdColdTime");
			MaxNpcCountInCamp = loader.GetInt("MaxNpcCountInCamp");
			NpcWorkRadius = loader.GetIntArray("NpcWorkRadius");
			DailyTradeTime = loader.GetIntArray("DailyTradeTime");
			DailyChestTime = loader.GetIntArray("DailyChestTime");
			LevelAddTime = loader.GetInt("LevelAddTime");
			LevelAddTimePrice = loader.GetIntArray("LevelAddTimePrice");
		}

		public readonly int ID;
		public readonly int[] InitialCoin;
		public readonly int[] InitialWood;
		public readonly int[] InitialGem;
		public readonly int[] InitialUserPower;
		public readonly int[] UserPowerRestore;
		public readonly int AdColdTime;
		public readonly int MaxNpcCountInCamp;
		public readonly int[] NpcWorkRadius;
		public readonly int[] DailyTradeTime;
		public readonly int[] DailyChestTime;
		public readonly int LevelAddTime;
		public readonly int[] LevelAddTimePrice;
	}


	public class ConfShop
	{
    	public static IDictionary<int, ConfShop> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfShop>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfShop(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfShop>();
			}
		}

		public static ConfShop Get(int id)
		{
			ConfShop conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfShop(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			Type = loader.GetEnum<ShopType>("Type");
			SceneType = loader.GetEnum<ShopSceneType>("SceneType");
			Scene = loader.GetInt("Scene");
			ItemShow = loader.GetInt("ItemShow");
			ItemGain = loader.GetInt("ItemGain");
			ItemGainBox = loader.GetInt("ItemGainBox");
			ItemOrder = loader.GetInt("ItemOrder");
			ItemLimit = loader.GetInt("ItemLimit");
			ItemUnlock = loader.GetInt("ItemUnlock");
			PriceType = loader.GetEnum<ShopPriceType>("PriceType");
			Price = loader.GetInt("Price");
			PriceRule = loader.GetEnum<ShopPriceRule>("PriceRule");
			PriceIncrease = loader.GetIntArray("PriceIncrease");
			PriceLimit = loader.GetInt("PriceLimit");
		}

		public readonly int ID;
		public readonly ShopType Type;
		public readonly ShopSceneType SceneType;
		public readonly int Scene;
		public readonly int ItemShow;
		public readonly int ItemGain;
		public readonly int ItemGainBox;
		public readonly int ItemOrder;
		public readonly int ItemLimit;
		public readonly int ItemUnlock;
		public readonly ShopPriceType PriceType;
		public readonly int Price;
		public readonly ShopPriceRule PriceRule;
		public readonly int[] PriceIncrease;
		public readonly int PriceLimit;
	}


	public class ConfShopCharge
	{
    	public static IDictionary<int, ConfShopCharge> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfShopCharge>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfShopCharge(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfShopCharge>();
			}
		}

		public static ConfShopCharge Get(int id)
		{
			ConfShopCharge conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfShopCharge(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			Type = loader.GetEnum<ChargeType>("Type");
			Classfication = loader.GetEnum<ChargeClassification>("Classfication");
			Name = loader.GetString("Name");
			Price = loader.GetFloat("Price");
			OriginPrice = loader.GetFloat("OriginPrice");
			GooglePlayBundle = loader.GetString("GooglePlayBundle");
			AppleStoreBundle = loader.GetString("AppleStoreBundle");
			Icon = loader.GetString("Icon");
			Effect = loader.GetString("Effect");
			Limit = loader.GetInt("Limit");
			Value = loader.GetInt("Value");
			Label = loader.GetString("Label");
			Order = loader.GetInt("Order");
			RewardGem = loader.GetInt("RewardGem");
			RewardItem = loader.GetIdCountArray("RewardItem");
		}

		public readonly int ID;
		public readonly ChargeType Type;
		public readonly ChargeClassification Classfication;
		public readonly string Name;
		public readonly float Price;
		public readonly float OriginPrice;
		public readonly string GooglePlayBundle;
		public readonly string AppleStoreBundle;
		public readonly string Icon;
		public readonly string Effect;
		public readonly int Limit;
		public readonly int Value;
		public readonly string Label;
		public readonly int Order;
		public readonly int RewardGem;
		public readonly ConfigIdCount[] RewardItem;
	}


	public class ConfShopTrade
	{
    	public static IDictionary<int, ConfShopTrade> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfShopTrade>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfShopTrade(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfShopTrade>();
			}
		}

		public static ConfShopTrade Get(int id)
		{
			ConfShopTrade conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfShopTrade(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			Order = loader.GetEnum<ShopTradeOrder>("Order");
			Item = loader.GetInt("Item");
			ItemLimit = loader.GetInt("ItemLimit");
			Price = loader.GetInt("Price");
			PriceIncrease = loader.GetInt("PriceIncrease");
			Power = loader.GetIntArray("Power");
			Prob = loader.GetInt("Prob");
		}

		public readonly int ID;
		public readonly ShopTradeOrder Order;
		public readonly int Item;
		public readonly int ItemLimit;
		public readonly int Price;
		public readonly int PriceIncrease;
		public readonly int[] Power;
		public readonly int Prob;
	}


	public class ConfTalk
	{
    	public static IDictionary<int, ConfTalk> Data;

		public static void Create(ConfigLoader loader)
		{
    		if (null != loader)
    		{
    			Data = new Dictionary<int, ConfTalk>(loader.Count);
    			while (loader.Next())
    			{
    				var conf = new ConfTalk(loader);
    				Data.Add(conf.ID, conf);
    			}
			}
			else
			{
    			Data = new Dictionary<int, ConfTalk>();
			}
		}

		public static ConfTalk Get(int id)
		{
			ConfTalk conf;
    		return Data.TryGetValue(id, out conf) ? conf : null;
		}

		public ConfTalk(ConfigLoader loader)
		{
			ID = loader.GetInt("ID");
			GroupID = loader.GetInt("GroupID");
			Order = loader.GetInt("Order");
			TalkText = loader.GetString("TalkText");
		}

		public readonly int ID;
		public readonly int GroupID;
		public readonly int Order;
		public readonly string TalkText;
	}

}