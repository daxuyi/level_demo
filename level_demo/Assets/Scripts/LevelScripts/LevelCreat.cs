﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Config;
using Game;
using Game.UI.Level.LevelMapNew;
using UnityEngine.UI;

public class LevelCreat : MonoBehaviour
{
    private Camera _mainCamera;
    private ScrollRect _scrollRect;
    private GameObject _content;
    private GameObject _prefabContent;
    private GameObject _prefab;
    private GameObject _canvas;
    private RectTransform _contentRect;
    private float _canvasHeight;
    private float _canvasWidth;
    private LevelMapItem[] _levelMapItems;
    private string[] _prefabNames;

    void Start()
    {
        InitMapItems();
    }


    private void InitMapItems()
    {
        ConfigManager.Create(true);
        _prefabContent = GameObject.Find("LevelContent");
        var prefabNames = GetMapPrefabNamesInOrder();
        _levelMapItems = new LevelMapItem[prefabNames.Length];

        for (int i = 0; i < prefabNames.Length; i++)
        {
            var prefabName = prefabNames[i];
            _prefab = Resources.Load<GameObject>("levelmap/levelmap/" + prefabName);
            _prefab = Instantiate(_prefab, _prefabContent.transform, false);
            _prefab.transform.localPosition = GetPrefabLocalPosition(prefabName);
        }
    }

    private Vector2 GetPrefabLocalPosition(string prefabName)
    {
        var confData = ConfLevelMap.Data;
        foreach (var kvp in confData)
        {
            if (kvp.Value.Prefab == prefabName)
            {
                var position = new Vector2(kvp.Value.PositionPort[0], kvp.Value.PositionPort[1]);
                position.y += 640f;
                return position;
            }
        }

        throw new Exception($"ConfLevelMap Do not contain prefabName:{prefabName}");
    }

    private string[] GetMapPrefabNamesInOrder()
    {
        var ret = new List<string>(45);

        foreach (var confLevelMap in ConfLevelMap.Data)
        {
            ret.Add(confLevelMap.Value.Prefab);
        }

        return ret.ToArray();
    }

    public class LevelMapPrefab
    {
        public readonly float height;
        public readonly LevelMapPoint[] levelMapPoints;

        public LevelMapPrefab(float height, LevelMapPoint[] levelMapPoints)
        {
            this.height = height;
            this.levelMapPoints = levelMapPoints;
        }
    }

    public class LevelMapPoint
    {
        public readonly Vector2 localPosition;

        public LevelMapPoint(Vector2 localPosition)
        {
            this.localPosition = localPosition;
        }
    }

    public static readonly Dictionary<string, LevelMapPrefab> LevelMapPrefabs = new Dictionary<string, LevelMapPrefab>()
    {
        {"prefab0", new LevelMapPrefab(677.999f, new LevelMapPoint[] { })},
        {
            "prefab1", new LevelMapPrefab(473.9982f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-4.294586f, -5.945618f)),
            })
        },
        {
            "prefab2", new LevelMapPrefab(515.0031f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(0f, -28.99994f)),
            })
        },
        {
            "prefab3", new LevelMapPrefab(520.0334f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-23f, 45f)),
            })
        },
        {
            "prefab4", new LevelMapPrefab(749.9556f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-133.7f, -8.5f)),
                new LevelMapPoint(new Vector2(85f, 80f)),
            })
        },
        {
            "prefab5", new LevelMapPrefab(514.983f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(10.69997f, -32.99976f)),
            })
        },
        {
            "prefab6", new LevelMapPrefab(474.0333f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(42f, -11f)),
            })
        },
        {
            "prefab7", new LevelMapPrefab(529.0333f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-35.2f, -17.99976f)),
            })
        },
        {
            "prefab8", new LevelMapPrefab(514.983f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(0f, -17f)),
            })
        },
        {
            "prefab9", new LevelMapPrefab(750.0142f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-92f, -2f)),
                new LevelMapPoint(new Vector2(109f, 98f)),
            })
        },
        {
            "prefab10", new LevelMapPrefab(529.0516f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(50f, 0f)),
            })
        },
        {
            "prefab11", new LevelMapPrefab(474.0335f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-33f, 0f)),
            })
        },
        {
            "prefab12", new LevelMapPrefab(515.0333f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(0f, 0f)),
            })
        },
        {
            "prefab13", new LevelMapPrefab(520.0352f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-32f, 56f)),
            })
        },
        {
            "prefab14", new LevelMapPrefab(473.9825f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(19f, -0.0083008f)),
            })
        },
        {
            "prefab15", new LevelMapPrefab(529.0007f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-34.5f, -3f)),
            })
        },
        {
            "prefab16", new LevelMapPrefab(519.9989f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(24f, 62.99994f)),
            })
        },
        {
            "prefab17", new LevelMapPrefab(515.0038f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(6.999969f, -19.99988f)),
            })
        },
        {
            "prefab18", new LevelMapPrefab(473.9857f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(39.00002f, -10f)),
            })
        },
        {
            "prefab19", new LevelMapPrefab(750.0009f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-98f, -23f)),
                new LevelMapPoint(new Vector2(135f, 57f)),
            })
        },
        {
            "prefab20", new LevelMapPrefab(519.9989f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(13.5f, 75.3f)),
            })
        },
        {
            "prefab21", new LevelMapPrefab(529.0007f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-30.00015f, -14.99756f)),
            })
        },
        {
            "prefab22", new LevelMapPrefab(473.9857f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(37f, 23f)),
            })
        },
        {
            "prefab23", new LevelMapPrefab(515.0038f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(6.999969f, -19.99988f)),
            })
        },
        {
            "prefab24", new LevelMapPrefab(519.9989f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(13.5f, 75.3f)),
            })
        },
        {
            "prefab25", new LevelMapPrefab(515.0038f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-3.479996f, -26.49976f)),
            })
        },
        {
            "prefab26", new LevelMapPrefab(473.9857f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-34f, -9f)),
            })
        },
        {
            "prefab27", new LevelMapPrefab(750.0009f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-128f, -21f)),
                new LevelMapPoint(new Vector2(109f, 54f)),
            })
        },
        {
            "prefab28", new LevelMapPrefab(529.0007f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(50f, -16f)),
            })
        },
        {
            "prefab29", new LevelMapPrefab(515.0038f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-3.479996f, -26.49976f)),
            })
        },
        {
            "prefab30", new LevelMapPrefab(473.9857f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(37f, -13f)),
            })
        },
        {
            "prefab31", new LevelMapPrefab(515.0038f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(6.999969f, -19.99988f)),
            })
        },
        {
            "prefab32", new LevelMapPrefab(473.9857f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-50f, -17f)),
            })
        },
        {
            "prefab33", new LevelMapPrefab(519.9989f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-18f, 61f)),
            })
        },
        {
            "prefab34", new LevelMapPrefab(750.0009f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-121f, -37f)),
                new LevelMapPoint(new Vector2(104f, 42f)),
            })
        },
        {
            "prefab35", new LevelMapPrefab(529.0007f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-37.99998f, -18.99999f)),
            })
        },
        {
            "prefab36", new LevelMapPrefab(473.9857f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(39.00002f, -10f)),
            })
        },
        {
            "prefab37", new LevelMapPrefab(519.9989f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-25.8f, 41.6f)),
            })
        },
        {
            "prefab38", new LevelMapPrefab(529.0007f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(50f, 0f)),
            })
        },
        {
            "prefab39", new LevelMapPrefab(519.9989f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-14f, 50f)),
            })
        },
        {
            "prefab40", new LevelMapPrefab(515.0038f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(6.999969f, -19.99988f)),
            })
        },
        {
            "prefab41", new LevelMapPrefab(750.0009f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-100f, -50f)),
                new LevelMapPoint(new Vector2(112f, 63f)),
            })
        },
        {
            "prefab42", new LevelMapPrefab(473.9857f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-45f, 15f)),
            })
        },
        {
            "prefab43", new LevelMapPrefab(519.9989f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(33f, 36f)),
            })
        },
        {
            "prefab44", new LevelMapPrefab(515.0038f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(6.999969f, -19.99988f)),
            })
        },
        {
            "prefab45", new LevelMapPrefab(473.9857f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-50f, 0f)),
            })
        },
        {
            "prefab46", new LevelMapPrefab(515.0038f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(6.999969f, -19.99988f)),
            })
        },
        {
            "prefab47", new LevelMapPrefab(529.0007f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(54f, -10f)),
            })
        },
        {
            "prefab48", new LevelMapPrefab(750.0009f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-134f, -22f)),
                new LevelMapPoint(new Vector2(102f, 27f)),
            })
        },
        {
            "prefab49", new LevelMapPrefab(519.9989f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-9f, 41f)),
            })
        },
        {
            "prefab50", new LevelMapPrefab(473.9857f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-50f, 4f)),
            })
        },
        {
            "prefab51", new LevelMapPrefab(519.9989f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(24f, 62.99994f)),
            })
        },
        {
            "prefab52", new LevelMapPrefab(750.0009f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-134f, -43f)),
                new LevelMapPoint(new Vector2(97f, 25f)),
            })
        },
        {
            "prefab53", new LevelMapPrefab(529.0007f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-40.5f, -8f)),
            })
        },
        {
            "prefab54", new LevelMapPrefab(750.0009f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-117f, -50f)),
                new LevelMapPoint(new Vector2(109f, 50f)),
            })
        },
        {
            "prefab55", new LevelMapPrefab(519.9989f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-37.8f, 48.7f)),
            })
        },
        {
            "prefab56", new LevelMapPrefab(473.9857f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(43f, -17f)),
            })
        },
        {
            "prefab57", new LevelMapPrefab(515.0038f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(6.999969f, -19.99988f)),
            })
        },
        {
            "prefab58", new LevelMapPrefab(529.0007f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(43f, -9f)),
            })
        },
        {
            "prefab59", new LevelMapPrefab(519.9988f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-18f, 49.999f)),
            })
        },
        {
            "prefab60", new LevelMapPrefab(473.9857f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(39.00002f, -10f)),
            })
        },
        {
            "prefab61", new LevelMapPrefab(515.0038f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(6.999969f, -19.99988f)),
            })
        },
        {
            "prefab62", new LevelMapPrefab(529.0007f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-37.99998f, -18.99999f)),
            })
        },
        {
            "prefab63", new LevelMapPrefab(750.0009f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-108f, -37f)),
                new LevelMapPoint(new Vector2(115f, 92f)),
            })
        },
        {
            "prefab64", new LevelMapPrefab(519.9988f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-18f, 49.999f)),
            })
        },
        {
            "prefab65", new LevelMapPrefab(473.9857f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(39.00002f, -10f)),
            })
        },
        {
            "prefab66", new LevelMapPrefab(515.0038f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(6.999969f, -19.99988f)),
            })
        },
        {
            "prefab67", new LevelMapPrefab(529.0007f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-38f, -19f)),
            })
        },
        {
            "prefab68", new LevelMapPrefab(473.9857f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(39.00002f, -10f)),
            })
        },
        {
            "prefab69", new LevelMapPrefab(519.9988f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-28f, 44.5f)),
            })
        },
        {
            "prefab70", new LevelMapPrefab(515.0038f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(6.999969f, -19.99988f)),
            })
        },
        {
            "prefab71", new LevelMapPrefab(750.0009f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-105f, -21f)),
                new LevelMapPoint(new Vector2(107f, 79f)),
            })
        },
        {
            "prefab72", new LevelMapPrefab(529.0007f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-37f, -4f)),
            })
        },
        {
            "prefab73", new LevelMapPrefab(473.9857f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-44f, 5f)),
            })
        },
        {
            "prefab74", new LevelMapPrefab(519.9988f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(38f, 55f)),
            })
        },
        {
            "prefab75", new LevelMapPrefab(750.0009f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-120f, -13f)),
                new LevelMapPoint(new Vector2(100f, 87f)),
            })
        },
        {
            "prefab76", new LevelMapPrefab(529.0007f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-37.99998f, -18.99999f)),
            })
        },
        {
            "prefab77", new LevelMapPrefab(515.0038f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(6.999969f, -19.99988f)),
            })
        },
        {
            "prefab78", new LevelMapPrefab(473.9857f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-37f, -78f)),
            })
        },
        {
            "prefab79", new LevelMapPrefab(519.9988f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(16f, 47f)),
            })
        },
        {
            "prefab80", new LevelMapPrefab(473.9857f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(50f, -16f)),
            })
        },
        {
            "prefab81", new LevelMapPrefab(515.0038f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(6.999969f, -19.99988f)),
            })
        },
        {
            "prefab82", new LevelMapPrefab(473.9857f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-50f, 10f)),
            })
        },
        {
            "prefab83", new LevelMapPrefab(519.9988f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(34f, 37f)),
            })
        },
        {
            "prefab84", new LevelMapPrefab(529.0007f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-37.99998f, -18.99999f)),
            })
        },
        {
            "prefab85", new LevelMapPrefab(473.9857f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(39.00002f, -10f)),
            })
        },
        {
            "prefab86", new LevelMapPrefab(515.0038f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(6f, -50.002f)),
            })
        },
        {
            "prefab87", new LevelMapPrefab(529.0007f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-46f, 9f)),
            })
        },
        {
            "prefab88", new LevelMapPrefab(519.9988f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-18f, 49.999f)),
            })
        },
        {
            "prefab89", new LevelMapPrefab(515.0038f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(6.999969f, -19.99988f)),
            })
        },
        {
            "prefab90", new LevelMapPrefab(473.9857f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-43f, 15f)),
            })
        },
        {
            "prefab91", new LevelMapPrefab(529.0007f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-36f, 0f)),
            })
        },
        {"prefab92", new LevelMapPrefab(519.9988f, new LevelMapPoint[] { })},
        {
            "prefab93", new LevelMapPrefab(257.0001f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(0f, 24.8f)),
            })
        },
        {
            "prefab94", new LevelMapPrefab(257.0001f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-3.051758E-05f, 84.6f)),
            })
        },
        {
            "prefab95", new LevelMapPrefab(257.0001f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-3.051758E-05f, 84.6f)),
            })
        },
        {
            "prefab96", new LevelMapPrefab(257.0001f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-3.051758E-05f, 84.6f)),
            })
        },
        {
            "prefab97", new LevelMapPrefab(257.0001f, new LevelMapPoint[]
            {
                new LevelMapPoint(new Vector2(-3.051758E-05f, 84.6f)),
            })
        },
    };
}