﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Level
{
    public class LevelCloudCreat : MonoBehaviour
    {
        private float _timer = 20f;
        private Camera _mainCamera;
        private CloudPool _cloudPool;
        private Vector2 _screenPointToUi;
        private GameObject _content;
        private GameObject _canvas;
        private RectTransform _contentRect;
        private float _canvasHeight;
        private float _canvasWidth;
        private int _screenIndexAtCanvas = 1;
        private Vector2 _startPos;
        private int cloudCreatRange = 2;
        private int _lastScreenIndexAtCanvas = 1;


        private void Start()
        {
            _mainCamera = Camera.main;

            _canvas = GameObject.Find("Canvas");
            _content = GameObject.Find("Content").gameObject;
            _contentRect = _content.GetComponent<RectTransform>();
            _canvasHeight = _canvas.GetComponent<RectTransform>().sizeDelta.y;
            _canvasWidth = _canvas.GetComponent<RectTransform>().sizeDelta.x;

            _cloudPool = GameObject.Find("CloudPool").GetComponent<CloudPool>();
            CreatCloudOnStart();
        }


        void Update()
        {
            GetScreenPointToUi();
            CreatCloud();
        }

        void CreatCloud()
        {
            if (_timer > 0)
            {
                _timer = _timer - Time.deltaTime;
            }
            else
            {
                CreatCloudOnNearbyScreen();
                _timer = 45f;
            }

            if (_lastScreenIndexAtCanvas != _screenIndexAtCanvas)
            {
                CreatCloudNew();
            }
        }

        void CreatCloudNew()
        {
            if (_lastScreenIndexAtCanvas > _screenIndexAtCanvas)
            {
                CreatCloudAtIndex(_lastScreenIndexAtCanvas - cloudCreatRange - 1);
            }
            else
            {
                CreatCloudAtIndex(_lastScreenIndexAtCanvas + cloudCreatRange + 1);
            }
        }

        void CreatCloudOnStart()
        {
            for (int i = _screenIndexAtCanvas - cloudCreatRange; i <= _screenIndexAtCanvas + cloudCreatRange; i++)
            {
                CreatCloudAtIndex(i);
            }
        }

        void CreatCloudOnNearbyScreen()
        {
            for (int i = _screenIndexAtCanvas - cloudCreatRange; i <= _screenIndexAtCanvas + cloudCreatRange; i++)
            {
                CreatFromLeftCloudAtIndex(i);
            }
        }

        void CloudInstantiate(Vector2 _startPos)
        {
            GameObject newCloud = _cloudPool.getCloud();
            newCloud.transform.SetParent(this.transform, false);
            newCloud.transform.localPosition = _startPos;
        }

        void CreatCloudAtIndex(int index)
        {
            // _startPos = new Vector2(Random.Range(-0.5f, 0.5f) * _canvasWidth,
            //     Random.Range(-0.8f, -0.20f) * _canvasHeight + index * _canvasHeight);
            // CloudInstantiate(_startPos);
            
            _startPos = new Vector2(Random.Range(-0.5f, 0.5f) * _canvasWidth,
                Random.Range(-0.9f, -0.60f) * _canvasHeight + index * _canvasHeight);
            CloudInstantiate(_startPos);
            
            _startPos = new Vector2(Random.Range(-0.5f, 0.5f) * _canvasWidth,
                Random.Range(-0.4f, -0.1f) * _canvasHeight + index * _canvasHeight);
            CloudInstantiate(_startPos);
        }

        void CreatFromLeftCloudAtIndex(int index)
        {
            // _startPos = new Vector2(-1.0f * _canvasWidth,
            //     Random.Range(-1f, 0f) * _canvasHeight + index * _canvasHeight);
            // CloudInstantiate(_startPos);
            
            
            
            _startPos = new Vector2(-1.0f * _canvasWidth,
                Random.Range(-0.8f, -0.70f) * _canvasHeight + index * _canvasHeight);
            CloudInstantiate(_startPos);
            
            _startPos = new Vector2(-1.0f * _canvasWidth,
                Random.Range(-0.2f, -0.3f) * _canvasHeight + index * _canvasHeight);
            CloudInstantiate(_startPos);
            

            // if (Random.Range(0,1f)<0.5f)
            // {
            //     _startPos = new Vector2(-1.0f * _canvasWidth,
            //         Random.Range(-0.8f, -0.7f) * _canvasHeight + index * _canvasHeight);
            //     CloudInstantiate(_startPos);
            //
            //     _startPos = new Vector2(-1.0f * _canvasWidth,
            //         Random.Range(-0.3f, -0.2f) * _canvasHeight + index * _canvasHeight);
            //     CloudInstantiate(_startPos);
            // }
            // else
            // {
            //     _startPos = new Vector2(-1.0f * _canvasWidth,
            //         Random.Range(-0.8f, 0.2f) * _canvasHeight + index * _canvasHeight);
            //     CloudInstantiate(_startPos);
            // }
        }

        void GetScreenPointToUi()
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_contentRect, Vector2.zero, _mainCamera,
                out _screenPointToUi);
            _lastScreenIndexAtCanvas = _screenIndexAtCanvas;
            _screenIndexAtCanvas = (int) (_screenPointToUi.y / _canvasHeight + 1.0f);
        }

        public int GetScreenIndex()
        {
            return _screenIndexAtCanvas;
        }

        public int GetRange()
        {
            return cloudCreatRange;
        }
    }
}