﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudPool : MonoBehaviour
{
    public GameObject[] _cloudPrefabs;

    private GameObject _cloud;
    private Queue<GameObject> _cloudPool;
    private int availableNum = 0;

    void Start()
    {
        _cloudPool=new Queue<GameObject>();
        for (int i = 0; i < 10; i++)
        {
            _cloud = Instantiate(_cloudPrefabs[Random.Range(0, 4)]);
            _cloud.SetActive(false);
            _cloudPool.Enqueue(_cloud);
            availableNum++;
        }
    }

    public GameObject getCloud()
    {
        if (availableNum <= 0)
        {
            _cloud = Instantiate(_cloudPrefabs[Random.Range(0, 5)]);
            return _cloud;
        }
        else
        {
            _cloud = _cloudPool.Dequeue();
            _cloud.SetActive(true);
            availableNum--;
            return _cloud;
        }
    }

    public void recovery(GameObject gameObject)
    {
        gameObject.SetActive(false);
        _cloudPool.Enqueue(gameObject);
        availableNum++;
    }
}