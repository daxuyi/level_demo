﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Level;

public class CloudMove : MonoBehaviour
{
    private float _moveSpeed = 0.1f;

    private Vector3 _startPoint;
    private Camera _mainCamera;
    private Vector3 _screenStartPoint;
    private Vector3 _screenEndPoint;
    private Vector3 _screenVectorStartToEnd;
    private CloudPool _cloudPool;
    private int _screenIndexAtCanvas;
    private int _cloudCreatRange;
    private LevelCloudCreat _levelCloudCreat;
    private float _canvasHeight;
    private float _canvasWidth;
    private int _index;

    private void Start()
    {
        _mainCamera = Camera.main;

        GameObject canvas = GameObject.Find("Canvas");
        _canvasHeight = canvas.GetComponent<RectTransform>().sizeDelta.y;
        _canvasWidth = canvas.GetComponent<RectTransform>().sizeDelta.x;

        _cloudPool = GameObject.Find("CloudPool").GetComponent<CloudPool>();
        _levelCloudCreat = GameObject.Find("CloudLayer").GetComponent<LevelCloudCreat>();
    }

    void Update()
    {
        Move();
        DestroyWhenOut();
    }

    void Move()
    {
        this.transform.position = new Vector3(this.transform.position.x + _moveSpeed * Time.deltaTime,
            this.transform.position.y + _moveSpeed * Time.deltaTime, 0);
    }

    void DestroyWhenOut()
    {
        _index = (int) (this.transform.localPosition.y / _canvasHeight + 1.0f);
        if (_index < _levelCloudCreat.GetScreenIndex() - _levelCloudCreat.GetRange() ||
            _index > _levelCloudCreat.GetScreenIndex() + _levelCloudCreat.GetRange() ||
            this.transform.localPosition.x > 1f * _canvasWidth)
        {
            _cloudPool.recovery(this.gameObject);
        }
    }
}