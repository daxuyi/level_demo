﻿
using UnityEngine;

namespace Config
{
    public class ConfigManager
    {
        public static ConfigManager Instance { get; private set; }

        public static bool Create(bool isDefault)
        {
            Instance = new ConfigManager();
            return Instance.Init(isDefault);
        }
        
        public static bool CreateInEditor(bool isDefault)
        {
            Instance = new ConfigManager();
            return Instance.InitForMapEditor(isDefault);
        }

        public static void Destroy()
        {
            Instance = null;
            ConfigLoader.UnInit();
        }
        
        public static string ConfigResPath { get; set; } = "Assets/StandaloneAssets/data/config.bytes";
        
        private ConfigManager()
        {
            ConfigLoader.Init();
        }

        private bool Init(bool isDefault)
        {
            var asset = UnityEditor.AssetDatabase.LoadAssetAtPath<TextAsset>(ConfigResPath);
            return LoadConfig(asset);
        }
        
        private bool InitForMapEditor(bool isDefault)
        {
            #if UNITY_EDITOR
            var asset = UnityEditor.AssetDatabase.LoadAssetAtPath<TextAsset>(ConfigResPath);
            if (asset != null)
            {
                LoadConfig(asset);
            }
            else
            {
                byte[] data = System.IO.File.ReadAllBytes(ConfigResPath);
                LoadConfig(data);
            }
            #endif
            return true;
        }

        private bool LoadConfig(TextAsset asset)
        {   
            if (ReferenceEquals(asset, null))
            {
                Debug.LogError("Load config failed!!!");
                return false;
            }

            LoadConfig(asset.bytes);
            
            Resources.UnloadAsset(asset);
            return true;
        }

        private bool LoadConfig(byte[] datas)
        {
            using (var ba = new ByteArray(datas))
            {
                while (ba.ReadAvailable)
                {
                    var file = ba.ReadUTF();
                    var count = ba.ReadInt();
                    var length = ba.ReadInt();
                    var bytes = ba.ReadBytes(length);

                    var loaderItem = ConfigLoader.GetLoaderItem(file);
                    if (loaderItem != null && length > 0)
                    {
                        ConfigLoader.CreateConfig(bytes, loaderItem.ItemFile, count, loaderItem.Callback);
                    }
                    else
                    {
                        if (loaderItem == null)
                        {
                            Debug.LogWarning($"Config {file} cannot load!!!");
                        }

                        if (length == 0)
                        {
                            Debug.LogWarning($"Config {file} has no data!!!");
                        }
                    }
                }
            }
            return true;
        }
    }
}